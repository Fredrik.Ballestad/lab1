package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean bool = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (bool == true) {
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");

            boolean valid_input = false;

            String player_choice = "";

            while (valid_input == false) {
                player_choice = sc.nextLine();

                if (player_choice.equals("rock") || player_choice.equals("paper") || player_choice.equals("scissors")) {
                    valid_input = true;
                }
                else {
                    System.out.println("I do not understand " + player_choice + ". Could you try again?");
                    valid_input = false;
                }
            }
            Random rand = new Random();
            String robot_choice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            System.out.print("Human chose " + player_choice + ", computer chose " + robot_choice + ". ");

            if (player_choice.equals(robot_choice)) {
                System.out.println("It's a tie!");
            } else if (player_choice.equals("rock") && robot_choice.equals("paper")) {
                System.out.println("Computer wins");
                computerScore = computerScore + 1;
            } else if (player_choice.equals("rock") && robot_choice.equals("scissors")) {
                System.out.println("Human wins!");
                humanScore = humanScore + 1;
            } else if (player_choice.equals("paper") && robot_choice.equals("rock")) {
                System.out.println("Human wins!");
                humanScore = humanScore + 1;
            } else if (player_choice.equals("paper") && robot_choice.equals("scissors")) {
                System.out.println("Computer wins");
                computerScore = computerScore + 1;
            } else if (player_choice.equals("scissors") && robot_choice.equals("rock")) {
                System.out.println("Human wins!");
                computerScore = computerScore + 1;
            } else if (player_choice.equals("scissors") && robot_choice.equals("paper")) {
                System.out.println("Human wins!");
                humanScore = humanScore + 1;
            }
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String quit_or_continue = sc.nextLine();
            if (quit_or_continue.equals("y")) {
                bool = true;
            } else if (quit_or_continue.equals("n")) {
                System.out.println("Bye bye :)");
                bool = false;
            }
            roundCounter++;

            // TODO: Implement Rock Paper Scissors
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
